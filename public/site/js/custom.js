$(window).load(function() {
	$("#preloader").delay(1000).fadeOut("slow");
	$("#status").delay(1000).fadeOut("slow");
})
$(document).ready(function(){
	$('.carousel.carousel-slider').carousel({
		fullWidth: true,
		indicators: true
	});
});

$(document).ready(function(){
	$('.event-calendar').equinox({
		events: [{ 
			class: '',
			color: '#000',
			data: {}
		}],
	});
}); 
$(document).ready(function(){
	$('.nav-menu li').click(function(){
		$('.nav-menu li').removeClass('active');
		$(this).addClass('active');
	});
});