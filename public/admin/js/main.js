$(document).ready(function() {
	$('.user_dropdown').dropdown();
	$('.datepicker').datepicker({
    format: 'dd/mm/yyyy'
  });
	$('.timepicker').timepicker();
	$('.collapsible').collapsible();
	$('select').formSelect();
});


$(function() { 
  $(".add_fixtures_form").validate({ 
    errorElement: 'span',
    rules: {  
      name_fixtures: "required", 
      home_team_name: "required",
      away_team_name: "required",
      date_match: "required", 
      stadium_match: "required",
      time_match: "required"
    }, 
    messages: {
      name_fixtures: "Tên đội bóng không được để trống", 
      home_team_name: "Tên đội nhà không được để trống",
      away_team_name: "Tên đội khách không được để trống",
      date_match: "Ngày thi đấu không được để trống", 
      stadium_match: "Địa điểm không được để trống",
      time_match: "Không được để trống"
    }, 
    submitHandler: function(form) {
      form.submit();
    }
  });

  $(".add_team_form").validate({ 
    errorElement: 'span',
    rules: {  
      name_team: "required", 
      played: "required",
      team_win: "required",
      team_drawn: "required", 
      team_lose: "required", 
      goal_scored: "required",
      goal_conceded: "required",
      sum_goal: "required"
    }, 
    messages: {
      name_team: "Tên đội bóng không được để trống", 
      played: "Tổng số trận không được để trống",
      team_win: "Số trận thắng không được để trống",
      team_drawn: "Số trận hòa không được để trống", 
      team_lose: "Số trận thua không được để trống", 
      goal_scored: "Số bàn thắng không được để trống",
      goal_conceded: "Số bàn thua không được để trống",
      sum_goal: "Hiệu số không được để trống"
    }, 
    submitHandler: function(form) {
      form.submit();
    }
  });
});