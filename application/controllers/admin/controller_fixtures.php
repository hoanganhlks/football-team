<?php 

class Controller_fixtures extends My_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/fixtures_model'); 
	}

	function add_fixtures()
	{
		$this->load->model('admin/league_model');
		$this->load->model('admin/team_model');
		$list_league = $this->league_model->get_list();
		$list_team = $this->team_model->get_list();  
		$data = array();
		$data['temp'] = 'admin/fixtures/add_fixtures';  
		$data['list_league'] = $list_league; 
		$data['list_team'] = $list_team;   

		if($this->input->post())
		{ 

			$data_logo_away = array(); 
			$logo_team_away = ''; 
			$config['upload_path']		= 	'./uploads/logo_team';
			$config['allowed_types']	= 	'jpg|png|jpeg|gif';
			$config['max_size']			= 	5120;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('logo_team_away')) {
				$error = array('error' => $this->upload->display_errors());
			}else {
				$logo_team_away =  preg_replace("/\s+/", "-", $this->normalizeString($_FILES['logo_team_away']['name']));
				$data_logo_away = array('upload_data' => $this->upload->data());
			} 


			$name_fixtures = $this->input->post('name_fixtures');
			$id_league = $this->input->post('cat_league'); 
			$id_team_home = $this->input->post('cat_team_home'); 
			$logo_team_away = $logo_team_away;
			$name_team_away = $this->input->post('name_team_away');  
			$goal_home = $this->input->post('goal_home'); 
			$goal_away = $this->input->post('goal_away');
			$date_match = $this->input->post('date_match');
			$time_games = $this->input->post('time_games');
			$stadium_match = $this->input->post('stadium_match');
			$time_match = $this->input->post('time_match'); 



			$input = array(
				'name_fixtures'=>$name_fixtures,
				'id_league'=>$id_league,
				'id_team_home'=>$id_team_home,
				'logo_team_away' =>$logo_team_away,
				'name_team_away'=>$name_team_away, 
				'goal_home'=>$goal_home, 
				'goal_away'=>$goal_away,
				'date_match'=>$date_match,
				'time_games'=>$time_games,
				'stadium_match'=>$stadium_match,
				'time_match'=>$time_match
			);
			$this->fixtures_model->create($input);
			$this->session->set_flashdata('mess','Đã thêm thành công');

		}
		$this->load->view('admin/index',$data);
	}   

	public static function normalizeString ($str = '') {
		$str = strip_tags($str);
		$str = preg_replace('/[\r\n\t ]+/', ' ', $str);
		$str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
		$str = strtolower($str);
		$str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
		$str = htmlentities($str, ENT_QUOTES, "utf-8");
		$str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
		$str = str_replace(' ', '_', $str);
		$str = rawurlencode($str);
		$str = str_replace('%', '-', $str);
		return $str;
	}

	function list_fixtures()
	{    
		$this->load->library('pagination'); 
		$this->db->select('*');
		$this->db->from('fc-fixtures-results'); 
		$this->db->order_by('id', 'asc'); 
		$offset=$this->uri->segment(2);    
		$limit= 5;        
		$this->db->limit($limit, $offset);   
		$config['base_url'] = site_url() . '/lich-thi-dau/';
		$config['total_rows'] = $this->db->count_all('fc-fixtures-results');
		$config['uri_segment']  = 2;
		$config['per_page'] = $limit;
		$config['prev_link']  = '&lt;';
		$config['next_link']  = '&gt;';
		$config['last_link']  = 'Cuối';
		$config['first_link'] = 'Đầu';
		$this->pagination->initialize($config);
		$paginator=$this->pagination->create_links(); 

		$data = array();   
		$list_fixtures = $this->fixtures_model->getDataFixtures();     
		$data['list_fixtures'] = $list_fixtures;  
		$data['paginator'] = $paginator; 

		$data['temp'] = 'admin/fixtures/list_fixtures';
		$this->load->view('admin/index',$data);
	}
 	
 	function edit_fixtures()
	{
		$data = array();
		$id = $this->uri->segment(4); 
		$row = $this->fixtures_model->get_info($id); 
		$data['row'] = $row;
		$data['temp'] = 'admin/fixtures/edit_fixtures';

		if($this->input->post())
		{ 	  
			 
			$data_logo_away = array(); 
			$logo_team_away = ''; 
			$config['upload_path']		= 	'./uploads/logo_team';
			$config['allowed_types']	= 	'jpg|png|jpeg|gif';
			$config['max_size']			= 	5120;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('logo_team_away')) {
				$error = array('error' => $this->upload->display_errors());
			}else {
				$logo_team_away =  preg_replace("/\s+/", "-", $this->normalizeString($_FILES['logo_team_away']['name']));
				$data_logo_away = array('upload_data' => $this->upload->data());
			} 

 
			$logo_team_away = $logo_team_away;
			$name_team_away = $this->input->post('name_team_away');  
			$goal_home = $this->input->post('goal_home'); 
			$goal_away = $this->input->post('goal_away');
			$date_match = $this->input->post('date_match'); 
			$time_match = $this->input->post('time_match');
			$stadium_match = $this->input->post('stadium_match');
			$review_match = $this->input->post('review_match'); 



			$input = array(    
				'name_team_away'=>$name_team_away, 
				'logo_team_away'=>$logo_team_away,
				'goal_home'=>$goal_home, 
				'goal_away'=>$goal_away,
				'date_match'=>$date_match, 
				'time_match'=>$time_match,
				'stadium_match'=>$stadium_match,
				'review_match'=>$review_match
			);

			$this->fixtures_model->update($id,$input);
			$this->session->set_flashdata('mess','Đã sửa thành công'); 
			redirect(admin_url('controller_fixtures/list_fixtures')); 

		}  
		$this->load->view('admin/index',$data);


	}
}
?>