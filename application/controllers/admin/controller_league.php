<?php 
	
	class Controller_league extends My_Controller
	{
		
		function __construct()
		{
			parent::__construct();
			$this->load->model('admin/league_model');
		}

		function add_league()
		{
			$data = array();
			$data['temp'] = 'admin/league/view_league';
			$list = $this->league_model->get_list(); 
			$data['list'] = $list;
			// neu submit form mà co du lieu post len
			if($this->input->post())
			{
				$this->form_validation->set_rules('name','League Name','required');	
	
				//khi nhập liệu chính xác
				if($this->form_validation->run())
				{
					$nameleague = $this->input->post('name');
					$input = array(
						'name'=>$nameleague
						);
					$this->league_model->create($input);
					$this->session->set_flashdata('mess','thành công');
					}		
				}
				$this->load->view('admin/index',$data);
		}
		
		function edit_league()
		{
			$data = array();
			$id = $this->uri->segment(4); 
			$row = $this->league_model->get_info($id); 
			$data['temp'] = 'admin/league/edit_league';
			if($this->input->post())
			{
				$this->form_validation->set_rules('name','League Name','required');		
				//khi nhập liệu chính xác
				if($this->form_validation->run())
				{
					$nameleague = $this->input->post('name');

					$input = array(
						'name'=>$nameleague
						);
					$this->league_model->update($id,$input);
					$this->session->set_flashdata('mess','Đã sửa thành công');
					$row->name = $nameleague;
					redirect(admin_url('controller_league/add_league'));
				}		
			} 
			$data['row'] = $row;
			$this->load->view('admin/index',$data);

		}
		 
	}
?>