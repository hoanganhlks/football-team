<?php 

class Controller_player extends My_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/player_model');
	}

	function add_player()
	{ 
		$this->load->model('admin/team_model');
		$list_team = $this->team_model->get_list();  
		$data = array();
		$data['temp'] = 'admin/player/add_player';  
		$data['list_team'] = $list_team;  

		if($this->input->post())
		{ 

			$data_avatar = array(); 
			$avatar_player = ''; 
			$config['upload_path']		= 	'./uploads/avatar_player';
			$config['allowed_types']	= 	'jpg|png|jpeg|gif';
			$config['max_size']			= 	5120;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('avatar_player')) {
				$error = array('error' => $this->upload->display_errors());
			}else {
				$avatar_player =  preg_replace("/\s+/", "-", $this->normalizeString($_FILES['avatar_player']['name']));
				$data_avatar = array('upload_data' => $this->upload->data());
			} 

			$name_player = $this->input->post('name');
			$id_team = $this->input->post('cat_team');
			$avatar_player = $avatar_player;
			$squad_number = $this->input->post('squad_number'); 
			$goals = $this->input->post('goals');
			$birthday = $this->input->post('birthday');
			$position = $this->input->post('position');
			$desc_position = $this->input->post('desc_position'); 
			$assists = $this->input->post('assists'); 
			$girly = $this->input->post('girly');

			$input = array(
				'name'=>$name_player,
				'id_team'=>$id_team,
				'avatar_player'=>$avatar_player,
				'squad_number'=>$squad_number,
				'goals'=>$goals,
				'birthday'=>$birthday,
				'position'=>$position,
				'desc_position'=>$desc_position, 
				'assists'=>$assists, 
				'girly'=>$girly
			);
			$this->player_model->create($input);
			$this->session->set_flashdata('mess','Đã thêm thành công');
		}
		$this->load->view('admin/index',$data);
	} 


	public static function normalizeString ($str = '') {
		$str = strip_tags($str);
		$str = preg_replace('/[\r\n\t ]+/', ' ', $str);
		$str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
		$str = strtolower($str);
		$str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
		$str = htmlentities($str, ENT_QUOTES, "utf-8");
		$str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
		$str = str_replace(' ', '_', $str);
		$str = rawurlencode($str);
		$str = str_replace('%', '-', $str);
		return $str;
	}

	function list_player()
	{
		$this->load->library('pagination');  
		$this->db->select('*');
		$this->db->from('fc-player'); 
		$this->db->order_by('id', 'asc'); 
		$offset=$this->uri->segment(2);    
		$limit= 5;        
		$this->db->limit($limit, $offset);   
		$config['base_url'] = site_url() . '/danh-sach-cau-thu/';
		$config['total_rows'] = $this->db->count_all('fc-player');
		$config['uri_segment']  = 2;
		$config['per_page'] = $limit;
		$config['prev_link']  = '&lt;';
		$config['next_link']  = '&gt;';
		$config['last_link']  = 'Cuối';
		$config['first_link'] = 'Đầu';
		$this->pagination->initialize($config);
		$paginator=$this->pagination->create_links(); 

		$data = array(); 
		$list_player = $this->player_model->list_player();
		$data['list_player'] = $list_player;
		$data['paginator'] = $paginator; 
		
		$data['temp'] = 'admin/player/list_player';  
		$this->load->view('admin/index',$data);      
	}  


	function edit_player()
	{
		$data = array();
		$id = $this->uri->segment(4); 
		$row = $this->player_model->get_info($id); 
		$data['row'] = $row;
		$data['temp'] = 'admin/player/edit_player';

		if($this->input->post())
		{ 	  
			$data_avatar = array(); 
			$avatar_player = ''; 
			$config['upload_path']		= 	'./uploads/avatar_player';
			$config['allowed_types']	= 	'jpg|png|jpeg|gif';
			$config['max_size']			= 	5120;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('avatar_player')) {
				$error = array('error' => $this->upload->display_errors());
			}else {
				$avatar_player =  preg_replace("/\s+/", "-", $this->normalizeString($_FILES['avatar_player']['name']));
				$data_avatar = array('upload_data' => $this->upload->data());
			} 


			$name_player = $this->input->post('name'); 
			$avatar_player = $avatar_player; 
			$birthday = $this->input->post('birthday');
			$squad_number = $this->input->post('squad_number');
			$goals = $this->input->post('goals'); 
			$position = $this->input->post('position');
			$desc_position = $this->input->post('desc_position'); 
			$assists = $this->input->post('assists'); 
			$girly = $this->input->post('girly');

			$input = array(
				'name'=>$name_player, 
				'avatar_player'=>$avatar_player, 
				'birthday'=>$birthday,
				'squad_number'=>$squad_number,
				'goals'=>$goals, 
				'position'=>$position,
				'desc_position'=>$desc_position, 
				'assists'=>$assists, 
				'girly'=>$girly
			);
			$this->player_model->update($id,$input);
			$this->session->set_flashdata('mess','Đã sửa thành công'); 
			redirect(admin_url('controller_player/list_player')); 

		}  
		$this->load->view('admin/index',$data);

	}
 


}
?>