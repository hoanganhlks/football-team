<?php 

class Controller_team extends My_Controller
{

	function __construct()
	{
		parent::__construct();
		$this->load->model('admin/team_model');
	}

	function add_team()
	{
		$this->load->model('admin/league_model');
		$list_league = $this->league_model->get_list();  
		$data = array();
		$data['temp'] = 'admin/teams/add_team';  
		$data['list_league'] = $list_league;  

		if($this->input->post())
		{ 

			$data_logo = array(); 
			$logo_team = ''; 
			$config['upload_path']		= 	'./uploads/logo_team';
			$config['allowed_types']	= 	'jpg|png|jpeg|gif';
			$config['max_size']			= 	5120;

			$this->load->library('upload', $config);

			if (!$this->upload->do_upload('logo_team')) {
				$error = array('error' => $this->upload->display_errors());
			}else {
				$logo_team =  preg_replace("/\s+/", "-", $this->normalizeString($_FILES['logo_team']['name']));
				$data_logo = array('upload_data' => $this->upload->data());
			} 

			$name_team = $this->input->post('name_team');
			$id_league = $this->input->post('cat_league');
			$logo_team = $logo_team;  
			$played = $this->input->post('played');
			$team_win = $this->input->post('team_win');
			$team_drawn = $this->input->post('team_drawn');
			$team_lose = $this->input->post('team_lose');
			$goal_scored = $this->input->post('goal_scored');
			$goal_conceded = $this->input->post('goal_conceded');
			$sum_goal = $this->input->post('sum_goal');  

			$input = array(
				'name_team'=>$name_team,
				'id_league'=>$id_league,
				'logo'=>$logo_team,
				'played'=>$played,
				'win'=>$team_win,
				'drawn'=>$team_drawn,
				'lose'=>$team_drawn, 
				'goal_scored'=>$goal_scored,
				'goal_conceded'=>$goal_conceded,
				'sum_goal'=>$sum_goal
			);
			$this->team_model->create($input);
			$this->session->set_flashdata('mess','Đã thêm thành công');

		}
		$this->load->view('admin/index',$data);
	} 


	public static function normalizeString ($str = '') {
		$str = strip_tags($str);
		$str = preg_replace('/[\r\n\t ]+/', ' ', $str);
		$str = preg_replace('/[\"\*\/\:\<\>\?\'\|]+/', ' ', $str);
		$str = strtolower($str);
		$str = html_entity_decode( $str, ENT_QUOTES, "utf-8" );
		$str = htmlentities($str, ENT_QUOTES, "utf-8");
		$str = preg_replace("/(&)([a-z])([a-z]+;)/i", '$2', $str);
		$str = str_replace(' ', '_', $str);
		$str = rawurlencode($str);
		$str = str_replace('%', '-', $str);
		return $str;
	}

	
	function table_league()
	{
		$data = array();
		$data['temp'] = 'admin/teams/table_league';   
		$this->load->view('admin/index',$data);
	}  
}
?>