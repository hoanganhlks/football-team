<?php
	/**
	 * 
	 */
	class Dashboard extends My_Controller
	{ 
		function __construct()
		{
			parent::__construct(); 
		}
		function index()
		{
			$data = array();
			$data['temp'] = 'admin/layout/dashboard';
			$this->load->view('admin/index',$data);
		}  
		
		function logout()
		{
			if($this->session->userdata('login'))
			{
				$this->session->unset_userdata('login');
				redirect(admin_url('controller_login/index')); 
			}
		} 
	}

?>