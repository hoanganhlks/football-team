<?php 
	class Controller_login extends MY_Controller
	{
		function __construct()
		{
			parent::__construct();
			$this->load->model('admin/login_model');
		}
		function index()
		{
			$data = array();			
			if($this->input->post())
			{
				//goi den ham kiem tra dang nhap check_login
				$this->form_validation->set_rules('login','login','callback_check_login');	
				if($this->form_validation->run())
				{
					$user_name = $this->input->post('user_name');
					//neu form da chay dung thi se tao 1 session cho admin
					$this->session->set_userdata('login',$user_name); 
					redirect(admin_url('dashboard/index'));
				}		
		
			}
			$this->load->view('admin/login/login',$data);
		}
		//so sanh tai khoan trong csdl,tra ve gia tri true false 
		function check_login()
		{
			$user_name = $this->input->post('user_name');
			$password = $this->input->post('password');
			$where = array('user_name'=>$user_name,'password'=>$password);
			if($this->login_model->check_exists($where))
			{
				return true;
			}
			else
			{ 
				$this->form_validation->set_message(__FUNCTION__,'Không đăng nhập thành công');
				return false;
			}
		}  
		
	}
?>