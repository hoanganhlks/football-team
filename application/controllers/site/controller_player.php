<?php
	/**
	 * 
	 */
	class Controller_player extends My_Controller
	{
		
		function __construct()
		{
			parent::__construct(); 
 			$this->load->model('site/player_model');
		}
		function index()
		{	
			
			$data = array(); 
			$list_player = $this->player_model->get_player_list();
			$top_scores = $this->player_model->top_scores();
			$data['list_player'] = $list_player; 
			$data['top_scores'] = $top_scores;  
			$data['temp'] = 'site/layout/list_player';
			$this->load->view('site/index',$data);
		} 
	}
?>