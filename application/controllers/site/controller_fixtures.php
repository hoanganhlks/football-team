<?php
	/**
	 * 
	 */
	class Controller_fixtures extends My_Controller
	{
		
		function __construct()
		{
			parent::__construct(); 
 			$this->load->model('site/fixtures_model');
		}
		function index()
		{	
			$this->load->library('pagination'); 
			$this->db->select('*');
			$this->db->from('fc-fixtures-results'); 
			$this->db->order_by('id', 'asc'); 
			$offset=$this->uri->segment(2);    
			$limit= 5;        
			$this->db->limit($limit, $offset);   
			$config['base_url'] = site_url() . '/lich-dau/';
			$config['total_rows'] = $this->db->count_all('fc-fixtures-results');
			$config['uri_segment']  = 2;
			$config['per_page'] = $limit;
			$config['prev_link']  = 'Trước';
			$config['next_link']  = 'Next'; 
			$this->pagination->initialize($config);
			$paginator=$this->pagination->create_links(); 

			$list_fixtures = $this->fixtures_model->list_fixtures(); 
			$data = array();
			$data['list_fixtures'] = $list_fixtures;
			$data['paginator'] = $paginator;  
			$data['temp'] = 'site/layout/fixtures_result';
			$this->load->view('site/index',$data);
		} 
	}
?>