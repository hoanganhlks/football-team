<?php
	/**
	 * 
	 */
	class Controller_home extends My_Controller
	{
		
		function __construct()
		{
			parent::__construct();
		}
		function index()
		{	
			$this->load->model('site/home_model');
			$next_match = $this->home_model->next_match();
			$lastest_result = $this->home_model->lastest_result();
			$list_fixtures = $this->home_model->list_fixtures(); 
			$list_player = $this->home_model->list_player(); 
			$top_scores = $this->home_model->top_scores();  
			$data = array();
			$data['next_match'] = $next_match;
			$data['lastest_result'] = $lastest_result;
			$data['list_fixtures'] = $list_fixtures;
			$data['list_player'] = $list_player;   
			$data['top_scores'] = $top_scores;  
 
			$data['temp'] = 'site/layout/home';
			$this->load->view('site/index',$data);
		} 
	}
?>