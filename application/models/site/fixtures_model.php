<?php

class Fixtures_model extends My_Model
{

	function __construct()
	{
		parent::__construct(); 
	}

	public function list_fixtures()
	{
		$this->db->select('fc-fixtures-results.id,fc-fixtures-results.name_fixtures,fc-fixtures-results.name_team_away,fc-fixtures-results.logo_team_away,fc-fixtures-results.date_match,fc-fixtures-results.time_match,fc-fixtures-results.goal_home,fc-fixtures-results.goal_away,fc-fixtures-results.stadium_match,fc-league.name,fc-team.name_team,fc-team.logo');
		$this->db->from('fc-fixtures-results');
		$this->db->join('fc-league', 'fc-fixtures-results.id_league=fc-league.id');
		$this->db->join('fc-team', 'fc-fixtures-results.id_team_home=fc-team.id');   
		$this->db->order_by('id', 'desc'); 
		$offset=$this->uri->segment(2);    
		$limit= 5;        
		$this->db->limit($limit, $offset); 
		$query = $this->db->get();    
		return $query->result();  
	}
}
?>