<?php

class Player_model extends My_Model
{

	function __construct()
	{
		parent::__construct();   
		$this->table = 'fc-player';
	}
 	
 	public function get_player_list()
 	{
 		$this->db->select('fc-player.id,fc-player.name,fc-player.squad_number,fc-player.birthday,fc-player.position,fc-player.position,fc-player.avatar_player');
		$this->db->from('fc-player'); 
		$this->db->order_by('squad_number', 'asc');  
		$query = $this->db->get();    
		return $query->result();  
 	}

 	public function top_scores()
	{
		$this->db->select('fc-player.id,fc-player.name,fc-player.goals');
		$this->db->from('fc-player'); 
		$this->db->order_by('goals', 'desc');
		$query = $this->db->get();    
		return $query->result(); 
	} 
}
?>