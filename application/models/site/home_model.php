<?php

class Home_model extends My_Model
{

	function __construct()
	{
		parent::__construct(); 
	}

	public function next_match()
	{
		$this->db->select('fc-fixtures-results.id,fc-fixtures-results.name_fixtures,fc-fixtures-results.name_team_away,fc-fixtures-results.logo_team_away,fc-fixtures-results.date_match,fc-fixtures-results.goal_home,fc-fixtures-results.goal_away,fc-fixtures-results.stadium_match,fc-fixtures-results.time_match,fc-league.name,fc-team.name_team,fc-team.logo');
		$this->db->from('fc-fixtures-results');
		$this->db->join('fc-league', 'fc-fixtures-results.id_league=fc-league.id');
		$this->db->join('fc-team', 'fc-fixtures-results.id_team_home=fc-team.id'); 
		$this->db->limit(1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();    
		return $query->result(); 
	}

	public function lastest_result()
	{
		$this->db->select('fc-fixtures-results.id,fc-fixtures-results.name_fixtures,fc-fixtures-results.name_team_away,fc-fixtures-results.logo_team_away,fc-fixtures-results.date_match,fc-fixtures-results.goal_home,fc-fixtures-results.goal_away,fc-fixtures-results.stadium_match,fc-fixtures-results.time_match,fc-fixtures-results.review_match,fc-league.name,fc-team.name_team,fc-team.logo');
		$this->db->from('fc-fixtures-results');
		$this->db->join('fc-league', 'fc-fixtures-results.id_league=fc-league.id');
		$this->db->join('fc-team', 'fc-fixtures-results.id_team_home=fc-team.id'); 
		$this->db->limit(1,1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();    
		return $query->result(); 
	}


	public function list_fixtures()
	{
		$this->db->select('fc-fixtures-results.id,fc-fixtures-results.name_fixtures,fc-fixtures-results.name_team_away,fc-fixtures-results.logo_team_away,fc-fixtures-results.date_match,fc-fixtures-results.goal_home,fc-fixtures-results.goal_away,fc-fixtures-results.stadium_match,fc-league.name,fc-team.name_team,fc-team.logo');
		$this->db->from('fc-fixtures-results');
		$this->db->join('fc-league', 'fc-fixtures-results.id_league=fc-league.id');
		$this->db->join('fc-team', 'fc-fixtures-results.id_team_home=fc-team.id'); 
		$this->db->limit(5, 1);
		$this->db->order_by('id', 'desc');
		$query = $this->db->get();    
		return $query->result(); 
	}

	public function list_player()
	{
		$this->db->select('fc-player.id,fc-player.name,fc-player.squad_number,fc-player.desc_position,fc-player.avatar_player');
		$this->db->from('fc-player');
		$this->db->limit(8);
		$this->db->order_by('squad_number', 'asc');
		$query = $this->db->get();    
		return $query->result(); 
	}

	public function top_scores()
	{
		$this->db->select('fc-player.id,fc-player.name,fc-player.goals');
		$this->db->from('fc-player');
		$this->db->limit(10);
		$this->db->order_by('goals', 'desc');
		$query = $this->db->get();    
		return $query->result(); 
	} 
}
?>