<?php
	
	class Fixtures_model extends My_Model
	{
		
		function __construct()
		{
			parent::__construct();
			$this->table = 'fc-fixtures-results';
		}

		public function getDataFixtures($input = array())
		{
 
			$this->db->select('fc-fixtures-results.id,fc-fixtures-results.name_fixtures,fc-fixtures-results.name_team_away,fc-fixtures-results.date_match,fc-fixtures-results.goal_home,fc-fixtures-results.goal_away,fc-league.name,fc-team.name_team');
			$this->db->from('fc-fixtures-results');
			$this->db->join('fc-league', 'fc-fixtures-results.id_league=fc-league.id');
			$this->db->join('fc-team', 'fc-fixtures-results.id_team_home=fc-team.id'); 
			$this->db->order_by('id', 'desc'); 
			$offset=$this->uri->segment(2);    
			$limit= 5;        
			$this->db->limit($limit, $offset); 
			$query = $this->db->get();    
			return $query->result();  
		}
 		
	}
?>