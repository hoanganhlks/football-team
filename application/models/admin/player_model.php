<?php

class Player_model extends My_Model
{

	function __construct()
	{
		parent::__construct();
		$this->table = 'fc-player';
	}

	public function list_player()
	{
		$this->db->select('*');
		$this->db->from('fc-player'); 
		$this->db->order_by('id', 'asc'); 
		$offset=$this->uri->segment(2);    
		$limit= 5;        
		$this->db->limit($limit, $offset); 
		$query = $this->db->get();    
		return $query->result();  
	} 
}
?>