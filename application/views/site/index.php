<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<link rel="icon" href="<?php echo public_url('site')?>/images/favicon.png" sizes="16x16">
<head>
	<?php $this->load->view('site/template/style'); ?>
</head>
<body  class="home page-template custom-background">  
	<div id="preloader"><div id="status"></div></div>
	<div class="hfeed site">
		<?php $this->load->view('site/template/header'); ?>
		<div id="content" class="site-content clearfix">
			<?php $this->load->view($temp) ?> 
		</div>
		<?php $this->load->view('site/template/footer'); ?>
	</div>
</body>
</html>