<div class="gdlr-result-by-month-wrapper gdlr-item"> 
	<h4 class="gdlr-result-by-month-header gdlr-first">Kết quả gần đây</h4>
	<div class="gdlr-result-by-month-year-wrap"> 
		<?php foreach($list_fixtures as $row) {?>
			<div class="result-in-month even">
				<div class="gdlr-result-date"><?php echo $row->date_match ?> - <?php echo $row->time_match ?></div>
				<div class="gdlr-result-match-team-wrapper">
					<span class="gdlr-result-match-team gdlr-left">
						<?php if ($row->logo != "") { ?>  
							<span class="gdlr-team-flag">
								<img src="<?php echo base_url()?>/uploads/logo_team/<?php echo $row->logo?>" alt="" width="123" height="141">
							</span>
						<?php } else { ?>  
							<span class="gdlr-team-flag">
								<img src="<?php echo base_url()?>/uploads/logo_team/default.png" alt="" width="123" height="141">
							</span>
						<?php } ?><?php echo $row->name_team?>
					</span>
					<?php if ($row->goal_home && $row->goal_away != "") { ?>  
						<span class="gdlr-result-match-versus"><?php echo $row->goal_home ?> - <?php echo $row->goal_away ?></span>
					<?php } else { ?>  
						<span class="gdlr-result-match-versus">VS</span>
					<?php } ?> 
					<span class="gdlr-result-match-team gdlr-right">
						<?php echo $row->name_team_away?>
						<?php if ($row->logo_team_away != "") { ?>  
							<span class="gdlr-team-flag">
								<img src="<?php echo base_url()?>/uploads/logo_team/<?php echo $row->logo_team_away?>" alt="" width="123" height="141">
							</span>
						<?php } else { ?>  
							<span class="gdlr-team-flag">
								<img src="<?php echo base_url()?>/uploads/logo_team/default.png" alt="" width="123" height="141">
							</span>
						<?php } ?>
					</span>
				</div>
				<a class="gdlr-result-read-more" href="#"><?php echo $row->stadium_match?></a>
				<div class="clear"></div>
			</div>   
		<?php }?> 
		<div class="paging"><?php echo $paginator; ?></div>
		<div class="event-calendar"></div>
	</div> 
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.18.1/moment.min.js"></script>