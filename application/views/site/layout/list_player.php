<div class="gdlr-result-by-month-wrapper gdlr-item">
	<h4 class="gdlr-result-by-month-header gdlr-first">Danh sách thành viên</h4> 
	<div class="ds-player-content">
		<div class="gdlr-result-by-player mc-carousel"> 
			<?php foreach($list_player as $row) {?>
				<div class="item">
					<div class="overlay"></div>
					<div class="mc-carousel__content">
						<h2 class="mc-carousel__title">
							<a href="#"><?php echo $row->name?></a>
						</h2>
						<div class="mc-carousel__parameters-block">
							<div class="model-height"><span>Số Áo</span><?php echo $row->squad_number?></div>
							<div class="model-bust"><span>Vị Trí</span><?php echo $row->position ?></div>
							<div class="model-waist"><span>Ngày Sinh</span><?php echo $row->birthday ?></div> 
						</div>								
					</div>
					<div class="mc-carousel__image">
						<?php if ($row->avatar_player != "") { ?>  
							<img class="beautyagency-model-carousel-1" src="<?php echo base_url()?>/uploads/avatar_player/<?php echo $row->avatar_player?>" alt="#">
						<?php } else { ?>  
							<img class="beautyagency-model-carousel-1" src="<?php echo public_url('site')?>/images/avatar.png" alt="#">
						<?php } ?>    
					</div>
				</div> 
			<?php }?>
		</div> 
		<div class="gdlr-result-top-scores">
			<aside class="widget widget_sportspress">
				<div class="panel lined">
					<div class="panel-content"><h1 style="text-align: left" class="vc_custom_heading"><span>TOP</span> Ghi Bàn</h1>
					</div>
				</div>
				<div class="sp-template sp-template-event-blocks">
					<div class="sp-table-wrapper">
						<table class="sp-data-playlist sp-paginated-table">
							<thead>
								<th>Cầu Thủ</th>
								<th class="text-right">Bàn Thắng</th>
							</thead>
							<tbody>
								<?php foreach($top_scores as $rows) {?>
									<tr class="sp-row">
										<td>
											<a href="#"><?php echo $rows->name?></a>
										</td>
										<td><?php echo $rows->goals?></td>
									</tr> 
								<?php }?>
							</tbody>
						</table>
					</div>
				</div>
			</aside> 
		</div>
	</div>
</div>