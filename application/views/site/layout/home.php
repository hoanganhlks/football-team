<div class="content-area content-area-left-sidebar" id="primary">
	<main class="site-main">
		<div class="slider-images cati-slider">
			<div class="panel  lined">
				<div class="panel-content">
					<h1 style="text-align: left" class="vc_custom_heading"><span>Hình ảnh</span> Team</h1>
				</div>
			</div> 
			<div class="cati-images">
				<div class="carousel carousel-slider">
					<a class="carousel-item" href="#one!"><img src="<?php echo public_url('site')?>/images/slider1.jpg"></a>
					<a class="carousel-item" href="#two!"><img src="<?php echo public_url('site')?>/images/slider2.jpg"></a>
					<a class="carousel-item" href="#three!"><img src="<?php echo public_url('site')?>/images/slider3.jpg"></a>
					<a class="carousel-item" href="#four!"><img src="<?php echo public_url('site')?>/images/bg.jpg"></a>
				</div>
			</div>
		</div>
		<div class="latest-results">
			<div class="wpb_wrapper">
				<div class="posts-list-wrapper">
					<div class="list-header">
						<div class="panel  lined">
							<div class="panel-content">
								<h1 style="text-align: left" class="vc_custom_heading"><span>Kết Quả</span> Mới Nhất</h1>
							</div>
						</div>  
						<div class="posts-list latest-match">
							<div class="entry latest-match-result">
								<?php foreach($lastest_result as $row) {?>
									<div class="entry-data">
										<div class="entry-header">
											<h2 class="entry-title"><a href="#" rel="bookmark"><?php echo $row->name_fixtures?></a></h2> 
											<div class="entry-meta">
												<div class="match-date-time height50">
													<div class="time-match">
														<span class="date"><i class="material-icons">date_range</i> <?php echo $row->date_match?></span>
														<span class="time"><i class="material-icons">access_time</i> <?php echo $row->time_match?></span>
													</div>
													<div class="stadium_match"><i class="material-icons">location_on</i><?php echo $row->stadium_match?></div>
												</div>
												<div class="match-info">
													<div class="team1 win">
														<?php if ($row->logo != "") { ?> 
															<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/<?php echo $row->logo?>');"></div> 
														<?php } else { ?> 
															<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/default.png');"></div>  
														<?php } ?>   
														<div class="name"><?php echo $row->name_team?></div>
													</div>
													<div class="match-score">
														<div class="score">Tỉ Số</div>
														<div class="data team1">
															<div class="team1 win"><?php echo $row->goal_home?></div>
															<div class="vs">VS</div>
															<div class="team2 loss"><?php echo $row->goal_away?></div>
														</div>
													</div>
													<div class="team2 loss">
														<?php if ($row->logo_team_away != "") { ?> 
															<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/<?php echo $row->logo_team_away?>');"></div> 
														<?php } else { ?> 
															<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/default.png');"></div>  
														<?php } ?>  
														<div class="name"><?php echo $row->name_team_away?></div>
													</div>
												</div>
											</div>
										</div> 
										<div class="entry-summary"><?php echo $row->review_match?></div>
										<div class="entry-footer">
											<div class="entry-more"> <a href="<?php echo base_url()?>lich-dau" class="more-link">Xem thêm</a></div>        
										</div>
									</div>    
								</div>
							<?php }?>
						</div>
					</div>
				</div>
			</div>
			<div class="player-list">
				<div class="panel  lined">
					<div class="panel-content">
						<h1 style="text-align: left" class="vc_custom_heading"><span>Danh Sách</span> Cầu Thủ</h1>
					</div>
				</div>   
				<div class="player-table-list">
					<ul>
						<?php foreach($list_player as $rows) {?>
							<li>
								<a href="#">
									<div class="player-item">
										<div class="player-img"> 
											<?php if ($rows->avatar_player != "") { ?> 
												<img src="<?php echo base_url()?>/uploads/avatar_player/<?php echo $rows->avatar_player?>">
											<?php } else { ?> 
												<img src="<?php echo base_url()?>/uploads/avatar_player/avatar.png">
											<?php } ?> 
										</div>
										<div class="player-info">
											<div class="player-squad"><?php echo $rows->squad_number?></div>
											<div class="player-name"><?php echo $rows->name?></div>
										</div>
										<span class="position_desc <?php echo $rows->desc_position?>"><?php echo $rows->desc_position?></span>
									</div>
								</a>
							</li> 
						<?php }?>
					</ul>
					<div style="text-align: right;margin: 10px 0;">
						<a href="<?php echo base_url()?>danh-sach-cau-thu">Xem tất cả</a>
					</div>
				</div>
			</div>
		</div>
	</main>
</div>
<div class="content-area-right-sidebar" id="second">
	<aside>
		<div class="wpb_column vc_column_container">
			<div class="wpb_wrapper">
				<div class="panel lined">
					<div class="panel-content"><h1 style="text-align: left" class="vc_custom_heading"><span>Trận Đấu</span> Sắp tới</h1>
					</div>
				</div>
				<div class="posts-list-wrapper ">
					<div class="posts-list home-next-match ">
						<div class="entry home-next-match azsc_match type-azsc_match status-publish">
							<?php foreach($next_match as $rows) {?>
								<div class="entry-data">  
									<div class="match-time-left">        
										<div class="time-left"> 
											<div id="time_coundount" class="time"></div> 
										</div> 
									</div> 
									<div class="match-info">
										<div class="team1 ">
											<?php if ($rows->logo != "") { ?> 
												<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/<?php echo $rows->logo?>');"></div> 
											<?php } else { ?> 
												<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/default.png');"></div>  
											<?php } ?>   
											<div class="name"><?php echo $rows->name_team ?></div>
										</div>
										<div class="vs">VS</div>
										<div class="team2 ">
											<?php if ($rows->logo_team_away != "") { ?> 
												<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/<?php echo $rows->logo_team_away?>');"></div> 
											<?php } else { ?> 
												<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/default.png');"></div>  
											<?php } ?>  
											<div class="name"><?php echo $rows->name_team_away ?></div>
										</div>
									</div>
									<div class="match-date-time">
										<div class="time-match">
											<span class="date"><i class="material-icons">date_range</i> <?php echo $rows->date_match?></span>
											<span class="time"><i class="material-icons">access_time</i> <?php echo $rows->time_match?></span>
										</div>
										<div class="stadium_match"><i class="material-icons">location_on</i><?php echo $rows->stadium_match?></div>
									</div>   
								</div>  

								<script> 
									var countDownDate = new Date("<?php echo $rows->date_match?> <?php echo $rows->time_match?>").getTime(); 
									
									var x = setInterval(function() { 
										var now = new Date().getTime(); 
										var distance = countDownDate - now; 
										var days = Math.floor(distance / (1000 * 60 * 60 * 24));
										var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
										var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
										var seconds = Math.floor((distance % (1000 * 60)) / 1000);

										document.getElementById("time_coundount").innerHTML = "<div class='days'><span class='count'>" + days + "</span><span class='title'>DAY</span></div>" + "<div class='hours'><span class='count'>" + hours + "</span><span class='title'>HRS</span></div>" + "<div class='minutes'><span class='count'>" + minutes + "</span><span class='title'>MIN</span></div>" + "<div class='seconds'><span class='count'>" + seconds + "</span><span class='title'>SEC</span></div>";

										if (distance < 0) {
											clearInterval(x);
											document.getElementById("time_coundount").innerHTML = "";
										}
									}, 1000);
								</script>
							<?php }?>  
						</div><!-- #post -->
					</div>
				</div>
			</div>
		</div>
	</aside> 
	<aside class="page type-page status-publish ">
		<div class="entry-content">
			<div class="homepage-widgets">  
				<aside class="widget widget_sportspress">
					<div class="panel  lined">
						<div class="panel-content">
							<h1 style="text-align: left" class="vc_custom_heading"><span>Kết Quả</span> Trận Đấu</h1>
						</div>
					</div> 
					<div class="sp-template sp-template-event-blocks">
						<div class="sp-table-wrapper"> 
							<div class="entry home-match-mini type-azsc_match  has-post-thumbnail hentry">
								<div class="entry-data"> 
									<?php foreach($list_fixtures as $rows) {?>
										<div class="match-date-time">
											<div class="match-info">
												<div class="team1 ">
													<?php if ($rows->logo != "") { ?>  
														<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/<?php echo $rows->logo?>');"></div>
													<?php } else { ?>  
														<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/default.png');"></div>
													<?php } ?>  
													<div class="name"><?php echo $rows->name_team?></div>
												</div>
												<div class="vs"><?php echo $rows->goal_home?> - <?php echo $rows->goal_away?></div>
												<div class="team2 ">
													<?php if ($rows->logo_team_away != "") { ?>  
														<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/<?php echo $rows->logo_team_away?>');"></div>
													<?php } else { ?>  
														<div class="logo" style="background-image: url('<?php echo base_url()?>/uploads/logo_team/default.png');"></div>
													<?php } ?>  
													<div class="name"><?php echo $rows->name_team_away?></div>
												</div>
											</div>  
										</div>
									<?php }?>
								</div>    
							</div>
						</div>
					</div> 
					<a href="<?php echo base_url()?>lich-dau" style="text-align: right;display: block;margin: 10px 0;color:#f90">Xem tất cả</a> 
				</aside>
			</div> 
		</div>
	</aside>
	<aside class="widget widget_sportspress">
		<div class="panel lined">
			<div class="panel-content"><h1 style="text-align: left" class="vc_custom_heading"><span>TOP</span> Ghi Bàn</h1>
			</div>
		</div>
		<div class="sp-template sp-template-event-blocks">
			<div class="sp-table-wrapper">
				<table class="sp-data-playlist sp-paginated-table">
					<thead>
						<th>Cầu Thủ</th>
						<th class="text-right">Bàn Thắng</th>
					</thead>
					<tbody>
						<?php foreach($top_scores as $rows) {?>
							<tr class="sp-row">
								<td>
									<a href="#"><?php echo $rows->name?></a>
								</td>
								<td><?php echo $rows->goals?></td>
							</tr> 
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>
	</aside> 	
	<div class="man_of_match">
		<img src="<?php echo public_url('site')?>/images/mom.png">
	</div>
</div> 