<footer class="site-footer">
	<div class="footer-container">
		<div class="footer-content clearfix">
			<div class="row" style="margin-bottom: 0">
				<div class="col s4">
					<div class="widget-footer">
						<div class="footer-title">Về Chúng Tôi</div>
						<div class="about-info">
							Cá Tính FC, nơi tập hợp những anh em có cùng một đam mê đó là bóng đá. Tuy chúng tôi không sinh cùng tháng cùng năm, cùng mẹ cùng cha nhưng đều cùng đến sân đúng giờ, cùng bùng cháy trong mỗi trận đấu.
						</div>
					</div>
				</div>
				<div class="col s4">
					<div class="widget-footer">
						<div class="footer-title">Hoạt động</div>
						<div class="about-info">
							<div class="ab-addr">
								<label>Trưng Vương Arena</label>
								<p>Địa chỉ: 403 Trưng Nữ Vương</p>
							</div>
							<div class="ab-addr">
								<label>Giờ Thi Đấu</label>
								<p>18:30 PM or 19:30 PM</p>
							</div>
						</div>
					</div>
				</div>
				<div class="col s4">
					<div class="widget-footer">
						 <div class="bg-img"></div>
					</div>
				</div>
			</div>
		</div>
		<div class="footer-coppyright">
			<span>&copy; 2018 CATI FC</span>
		</div>
	</div>
</footer>

<script type="text/javascript" src="<?php echo public_url('site')?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo public_url('site')?>/js/materialize.min.js"></script> 
<script type="text/javascript" src="<?php echo public_url('site')?>/js/equinox.min.js"></script> 
<script type="text/javascript" src="<?php echo public_url('site')?>/js/custom.js"></script> 