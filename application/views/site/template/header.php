<header class="site-header">
	<div class="header-area header-area-custom header-area-has-logo">
		<div class="site-branding">
			<div class="site-identity">
				<a class="site-logo" href="<?php echo base_url()?>trang-chu" rel="home"><img src="<?php echo public_url('site')?>/images/logo.png" alt="CATI FC"></a>
				<hgroup>
					<h1 class="site-title"><a href="<?php echo base_url()?>trang-chu" rel="home">Cá Tính FC</a></h1>
					<h4 class="site-description">Official Site</h4>
				</hgroup>
			</div>
		</div>
		<div class="site-menu">
			<nav id="site-navigation" class="main-navigation" role="navigation"> 
				<button class="menu-toggle" aria-controls="menu" aria-expanded="false">
					<span class="dashicons dashicons-menu"></span>
				</button>
				<div class="menu-primary-menu-container">
					<ul id="menu-primary-menu" class="menu nav-menu" aria-expanded="false">
						<li class="active"><a href="<?php echo base_url()?>trang-chu">Trang chủ</a></li>
						<li><a href="<?php echo base_url()?>lich-dau">Lịch Thi Đấu</a></li> 
						<li><a href="<?php echo base_url()?>thanh-vien">Danh sách cầu thủ</a></li>
						<li><a href="#">Thư Viện Ảnh</a></li>
						<li><a href="#">Tài Chính</a></li>
						<li><a href="#">Liên Hệ</a></li> 
						<li><a href="<?php echo base_url()?>dang-nhap">Đăng Nhập Hệ Thống</a></li> 
					</ul>
				</div>
			</nav>
		</div>
	</div>
</header>