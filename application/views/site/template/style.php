<meta charset="utf-8">
<title>Fc Cati | Official Site</title>
<link rel="stylesheet" href="<?php echo public_url('site')?>/css/animate.css"> 
<link rel="stylesheet" type="text/css" href="<?php echo public_url('site')?>/css/bootstrap.min.css">
<link rel="stylesheet" href="<?php echo public_url('site')?>/css/materialize.css">
<link rel="stylesheet" href="<?php echo public_url('site')?>/css/styles.css"> 
<link rel="stylesheet" href="<?php echo public_url('site')?>/css/equinox.css"> 
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700,800,900&amp;subset=vietnamese" rel="stylesheet">