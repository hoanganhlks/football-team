<main>
	<div class="row">
		<div class="col s6">
			<div style="padding: 35px;" align="center" class="card">
				<div class="row">
					<div class="left card-title">
						<b>Thêm giải đấu </b>
					</div> 
					<div class="alert">
						<span class="error"><?php echo form_error('name');?></span> 
						<span class="success"><?php $this->session->flashdata('mess');?></span>
					</div> 
				</div> 
				<div class="row add_league_title"> 
					<form class="col s12" method="post">
						<div class="row"> 
							<div class="input-field col s12">
								<i class="material-icons prefix">grain</i>
								<input name="name" id="name_league" type="text" class="validate" value="<?php set_value('name')?>">
								<label for="name_league">Tên Giải Đấu</label>
							</div>
							<div class="input-field col s12">
								<button class="waves-effect waves-light btn man-utd submit_form" name="ok">Submit</button>
							</div>
						</div> 
					</form>
				</div>
			</div>
		</div> 
		<div class="col s6">
			<div style="padding: 35px;" align="center" class="card">
				<div class="row">
					<div class="left card-title">
						<b>Danh sách giải đấu</b>
					</div> 
				</div> 
				<div class="row">
					<form class="col s12">
						<div class="row">
							<table class="striped custom-table">
								<thead>
									<tr>
										<th></th>
										<th>Tên Giải Đấu</th>
										<th style="text-align: center;">Chỉnh Sửa</th>
									</tr>
								</thead> 
								<tbody>
									<?php foreach($list as $row) {?>
										<tr> 
											<td><?php echo $row->id?></td>
											<td><?php echo $row->name?></td>
											<td style="text-align: center;"><a href="<?php echo admin_url('controller_league/edit_league/'.$row->id);?>"><i class="material-icons">edit</i></a></td> 
										</tr>  
									<?php }?> 
								</tbody>
							</table> 
						</div> 
					</form>
				</div>
			</div>
		</div>
	</div>
</main>