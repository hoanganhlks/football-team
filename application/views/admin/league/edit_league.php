<main>
	<div class="row">
		<div class="col s6">
			<div style="padding: 35px;" align="center" class="card">
				<div class="row">
					<div class="left card-title">
						<b>Sửa tên giải đấu</span></b>
					</div> 
					<div class="alert">
						<span class="error"><?php echo form_error('name');?></span> 
						<span class="success"><?php $this->session->flashdata('mess');?></span>
					</div> 
				</div> 
				<div class="row add_league_title"> 
					<form class="col s12" method="post">
						<div class="row"> 
							<div class="input-field col s12">
								<i class="material-icons prefix">grain</i>
					<input name="name" id="name_league" type="text" class="validate" value="<?php echo $row->name?>">
								<label for="name_league">Tên Giải Đấu</label>
							</div>
							<div class="input-field col s12">
								<button class="waves-effect waves-light btn submit_form">Submit</button>
							</div>
						</div> 
					</form>
				</div>
			</div>
		</div>  
	</div>
</main>