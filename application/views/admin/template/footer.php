<footer class="man-utd-black page-footer" style="background: #1a1a1a">
    <div class="container">
        <div class="row">
            <div class="col s12">
                <h5 class="white-text">FC Cati</h5>
                <ul id='credits'>
                    <li>
                        Gif Logo made using <a href="https://formtypemaker.appspot.com/" title="Form Type Maker">Form Type Maker</a> from <a href="https://github.com/romannurik/FORMTypeMaker" title="romannurik">romannurik</a>
                    </li>
                    <li>
                        Icons made by <a href="https://material.io/icons/">Google</a>, available under <a href="https://www.apache.org/licenses/LICENSE-2.0" target="_blank">Apache License Version 2.0</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <span>Made By Hoàng Anh</span>
        </div>
    </div>
</footer>
<script type="text/javascript" src="<?php echo public_url('admin')?>/js/jquery.min.js"></script>
<script type="text/javascript" src="<?php echo public_url('admin')?>/js/materialize.min.js"></script>
<script type="text/javascript" src="<?php echo public_url('admin')?>/js/jquery.validate.min.js"></script>
<script type="text/javascript" src="<?php echo public_url('admin')?>/js/main.js"></script>  