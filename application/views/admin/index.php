<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>
<link rel="icon" href="<?php echo public_url('admin')?>/images/favicon.png" sizes="16x16">
<head>
	<?php $this->load->view('admin/template/header'); ?>
</head>
<body>
	<?php $this->load->view('admin/layout/menu-left'); ?>
	<?php $this->load->view('admin/layout/top'); ?>
	<?php $this->load->view($temp) ?> 
	<?php $this->load->view('admin/template/footer'); ?>
</body>
</html>