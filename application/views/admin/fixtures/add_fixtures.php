<main>
	<div class="row">
		<div class="col s12">
			<div style="padding: 35px;" align="center" class="card">
				<div class="row">
					<div class="left card-title">
						<b>Thêm Lịch thi đấu </b>
					</div> 
					<div class="alert"> 
						<span class="success"><?php echo $this->session->flashdata('mess')?></span>
					</div> 
				</div> 
				<div class="row add_league_title"> 
					<form class="col s12 add_fixtures_form" method="post" action="<?php admin_url('admin/controller_fixtures/add_fixtures');?>" enctype="multipart/form-data">
						<div class="row"> 
							<div class="input-field col s6"> 
								<input name="name_fixtures" id="name_fixtures" type="text" class="validate">
								<label for="name_fixtures">Tên Vòng Đấu</label>
							</div>
							<div class="input-field col s6"> 
								<select name="cat_league">
									<option value="" disabled selected>Chọn giải đấu tham gia</option> 
									<?php foreach($list_league as $row) {?>
										<option value="<?php echo $row->id?>"><?php echo $row->name?></option> 
									<?php }?>
								</select>
								<label>Chọn giải đấu</label>
							</div>
						</div> 
						<div class="row name_team_row">
							<div class="input-field col s6"> 
								<select name="cat_team_home">
									<option value="" disabled selected>Chọn đội bóng</option> 
									<?php foreach($list_team as $row) {?>
										<option value="<?php echo $row->id?>"><?php echo $row->name_team?></option> 
									<?php }?>
								</select>
								<label>Chọn Đội Nhà</label>
							</div>
							<div class="input-field col s3"> 
								<input name="goal_home" id="goal_home" type="text" class="validate">
								<label for="goal_home">Bàn Thắng Đội Nhà</label>
							</div> 
							<div class="input-field col s3"> 
								<input name="goal_away" id="goal_away" type="text" class="validate">
								<label for="goal_away">Bàn Thắng Đội Khách</label>
							</div>
						</div>
						<div class="row upload-logo">
							<div class="input-field col s6">  
								<input name="name_team_away" id="name_team_away" type="text" class="validate">
								<label for="name_team_away">Tên Đội Khách</label>
							</div> 
							<div class="file-field input-field col s6"> 
								<div class="btn upload_logo_home">
									<span>Upload logo đội bóng</span>
									<input type="file" name="logo_team_away">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text" style="height: 3rem">
								</div> 
							</div> 
						</div> 
						<div class="row name_team_row">
							<div class="input-field col s4"> 
								<input type="text" class="datepicker" id="date_match" name="date_match">
								<label for="date_match">Ngày thi đấu</label>
							</div>
							<div class="input-field col s2"> 
								<input name="time_games" id="time_games" type="text" class="validate">
								<label for="time_games">Thời Gian</label>
							</div>
							<div class="input-field col s4"> 
								<input name="stadium_match" id="stadium_match" type="text" class="validate">
								<label for="stadium_match">Địa điểm</label>
							</div>
							<div class="input-field col s2"> 
								<input name="time_match" id="time_match" type="text" class="validate timepicker">
								<label for="time_match">Giờ thi đấu</label>
							</div>
						</div>
						<div class="row">
							<div class="input-field col s12">
								<button class="waves-effect waves-light btn man-utd" name="ok">Submit</button>
							</div>
						</div> 
					</form>
				</div>
			</div>
		</div>  
	</div>
</main>