<main>
	<div class="row">
		<div class="col s12">
			<div style="padding: 35px;" align="center" class="card">
				<div class="row">
					<div class="left card-title">
						<b>Lịch thi đâu</b>
					</div> 
					<div class="alert">
						<span class="error"></span> 
						<span class="success"></span>
					</div> 
				</div>  
				<div class="row season_table"> 
					<div class="col s12 table_league_preview">
						<table class="striped custom-table">
							<thead>
								<tr>
									<th style="text-align: center;">STT</th>
									<th>Tên Giải Đấu</th>
									<th>Tên Vòng</th>
									<th>Đội Nhà</th>
									<th>Đội Khách</th>
									<th>Ngày thi đấu</th> 
									<th>Tỉ số</th> 
									<th style="text-align: center;">Chỉnh Sửa</th>
								</tr>
							</thead> 
							<tbody> 
								<?php foreach($list_fixtures as $rows) {?>
								<tr> 
									<td style="text-align: center;"><?php echo $rows->id?></td>
									<td><?php echo $rows->name?></td>
									<td><?php echo $rows->name_fixtures?></td> 
									<td><?php echo $rows->name_team?></td>
									<td><?php echo $rows->name_team_away?></td>
									<td><?php echo $rows->date_match?></td>
									<td><?php echo $rows->goal_home?> - <?php echo $rows->goal_away?></td> 
									<td style="text-align: center;"><a href="<?php echo admin_url('controller_fixtures/edit_fixtures/'.$rows->id);?>"><i class="material-icons">edit</i></a></td> 
								</tr>  
								<?php }?>  
							</tbody>
						</table> 
						<div class="paging"><?php echo $paginator; ?></div> 
					</div>
				</div> 
			</div>
		</div>  
	</div>
</main>