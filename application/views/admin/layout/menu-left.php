<ul id="slide-out" class="side-nav fixed z-depth-2">
	<li class="center no-padding">
		<div class="man-utd  white-text" style="height: 180px;">
			<div class="row">
				<img style="margin-top: 5%;height: 100px;" src="<?php echo public_url('admin')?>/images/logo.png" class=" responsive-img" /> 
				<p style="margin-top: -13%;"></p>
			</div>
		</div>
	</li>

	<li id="dash_dashboard"><a class="waves-effect" href="<?php echo base_url()?>dashboard"><b>Bảng tin</b></a></li>

	<ul class="collapsible" data-collapsible="accordion">
		<li id="menu_admin">
			<div class="collapsible-header waves-effect"><b>Quản Lý Giải Đấu</b></div>
			<div class="collapsible-body">
				<ul>
					<li>
						<a class="waves-effect" style="text-decoration: none;" href="<?php echo base_url()?>them-mua-giai">Thêm Mùa Giải</a>
					</li> 
					<li>
						<a class="waves-effect" style="text-decoration: none;" href="<?php echo base_url()?>them-lich-thi-dau">Thêm Lịch Thi Đấu</a>
					</li>  
					<li>
						<a class="waves-effect" style="text-decoration: none;" href="<?php echo base_url()?>lich-thi-dau">Lịch Thi Đấu</a>
					</li>  
				</ul>
			</div>
		</li>

		<li id="dash_products">
			<div id="dash_products_header" class="collapsible-header waves-effect"><b>Đội Bóng</b></div>
			<div id="dash_products_body" class="collapsible-body">
				<ul>
					<li id="products_product">
						<a class="waves-effect" style="text-decoration: none;" href="<?php echo base_url()?>them-doi-bong">Thêm đội bóng</a>
						<a class="waves-effect" style="text-decoration: none;" href="<?php echo base_url()?>bang-xep-hang">Bảng xếp hạng</a>
					</li>
				</ul>
			</div>
		</li>

		<li id="dash_categories">
			<div id="dash_categories_header" class="collapsible-header waves-effect"><b>Cầu Thủ</b></div>
			<div id="dash_categories_body" class="collapsible-body">
				<ul>
					<li id="categories_category">
						<a class="waves-effect" style="text-decoration: none;" href="<?php echo base_url()?>them-cau-thu">Thêm Cầu Thủ</a>
					</li>

					<li id="categories_sub_category">
						<a class="waves-effect" style="text-decoration: none;" href="<?php echo base_url()?>danh-sach-cau-thu">Danh Sách Cầu Thủ</a>
					</li>
				</ul>
			</div>
		</li>
 
		<li id="dash_dashboard"><a class="waves-effect" href="<?php echo base_url('admin/dashboard/logout');?>"><b>Đăng Xuất</b></a></li>
	</ul>
</ul>