<?php $this->load->view('admin/template/header') ?>
<div class="tsw-layout-login">
	<div class="tsw-login-content">
		<div class="tsw-form_login"> 
			<div class="tsw-logo">
				<img src="<?php echo public_url('admin')?>/images/logo.png"> 
			</div>
			<div class="tsw-form-layout">
				<form method="post" action="<?php admin_url('controller_login/index');?>">
					<div class="input-field col s12">
						<i class="material-icons prefix">person</i>
						<input id="user_name" type="text" name="user_name" class="validate">
						<label for="user_name" class="">Tên Đăng Nhập</label>
					</div>
					<div class="input-field col s12">
						<i class="material-icons prefix">lock_outline</i>
						<input id="password" type="password" name="password" class="validate">
						<label for="password">Mật Khẩu</label>
					</div>
					<div class="tsw-forgot">
						<a href="<?php echo base_url()?>trang-chu">Quay lại trang chủ</a>
					</div>
					<div class="tsw-button">
						<button class="btn-tsw_main waves-effect waves-light">Đăng Nhập</button> 
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<style>

* {
	padding: 0;
	margin: 0;
}
.tsw-layout-login {
	display: flex;
	min-height: 100vh;
	width: 100%;
	background: url('<?php echo public_url('admin')?>/images/bg-login.jpg') center; 
	justify-content: center;
	align-items: center;
	background-size: cover;
	position: relative;
}
.tsw-layout-login:before {
	content: "";
	position: absolute;
	top: 0;
	right: 0;
	left: 0;
	bottom: 0;
	background: linear-gradient(50deg, HSLA(205, 72%, 7%, 1), #240007);
	opacity: 0.6;
}
.tsw-login-content {
	position: relative;
	display: block;
	width: 100%;
	max-width: 400px;
}

.tsw-form_login .tsw-logo {
	width: auto;
	display: block;
	text-align: center;
	margin-bottom: 20px;
}
.tsw-form_login .tsw-logo h4 {
	color: #777;
	font-weight: 300;
	text-transform: uppercase;
	font-size: 25px;
	margin-bottom: 40px;
}
.tsw-form_login .tsw-logo img {
	height: 125px;
}

.tsw-form-layout {
	width: 100%;
	display: block;
}

.tsw-form_login {
	width: 100%;
	display: block;
	background: rgb(255, 255, 255);
	padding: 30px 20px 50px;
	border-radius: 4px;
	-webkit-box-shadow: 0 16px 28px 0 rgba(0, 0, 0, 0.22), 0 25px 55px 0 rgba(0, 0, 0, 0.21);
	-moz-box-shadow: 0 16px 28px 0 rgba(0, 0, 0, 0.22), 0 25px 55px 0 rgba(0, 0, 0, 0.21);
	box-shadow: 0 16px 28px 0 rgba(0, 0, 0, 0.22), 0 25px 55px 0 rgba(0, 0, 0, 0.21);
}

.login-ic {
	position: relative;
	margin-bottom: 1.5em;
	padding: 5px;
	display: table;
	width: 100%;
	background: rgba(255, 255, 255, 0.68);
}
.login-ic i {
	width: 38px;
	height: 38px;
	float: left;
	display: inline-block;
	text-align: center;
	font-size: 18px;
	line-height: 38px;
}
.login-ic input[type="text"], .login-ic input[type="password"] {
	float: left;
	background: none;
	outline: none;
	font-size: 15px;
	font-weight: 400;
	padding: 10px 16px;
	border: none;
	border-left: 1px solid rgba(255, 255, 255, 0.35);
	width: 82%;
	display: inline-block;
	margin-left: 7px;
}
.tsw-forgot {
	text-align: right;
	display: block;
	margin-bottom: 15px;
}

.tsw-forgot a {
	color: #777;
}

.tsw-button {
	display: block;
}

.tsw-button a {
	width: 100%;
	color: #fff;
}
.tsw-form-layout .input-field label {
	font-weight: 400;
	color: #777;
}

.tsw-form-layout .input-field i {
	top: 1rem;
	color: #777;
}
.tsw-login-content.signUp_form {
	max-width: 700px;
}
.tsw-form-layout .col.s12 {
	margin: 0 0 10px;
}
.btn-tsw_main {
	background: #FF9800;
	color: #fff;
	text-transform: uppercase;
	font-weight: 600;
	display: inline-block;
	text-align: center;
	height: 45px;
	line-height: 45px;
	border: 0;
	width: 100%;
	outline: none;
}
.btn-tsw_main:focus {
	background: #0b1625;
}
footer {
	display: none;
}
</style>
<?php $this->load->view('admin//template/footer') ?>