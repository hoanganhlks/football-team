<main>
	<div class="row">
		<div class="col s12">
			<div style="padding: 35px;" align="center" class="card">
				<div class="row">
					<div class="left card-title">
						<b>Thêm Cầu Thủ </b>
					</div> 
					<div class="alert"> 
						<span class="success"><?php echo $this->session->flashdata('mess')?></span>
					</div> 
				</div> 
				<div class="row add_league_title"> 
					<form class="col s12 add_player_form" method="post" action="<?php admin_url('admin/controller_player/add_player');?>" enctype="multipart/form-data">
						<div class="row"> 
							<div class="input-field col s4"> 
								<input name="name" id="name" type="text" class="validate">
								<label for="name">Tên Cầu Thủ</label>
							</div>
							<div class="input-field col s4"> 
								<select name="cat_team">
									<option value="" disabled >Chọn đội bóng tham gia</option>
									<?php foreach($list_team as $row) {?>
									<option value="<?php echo $row->id?>" selected><?php echo $row->name_team?></option> 
									<?php }?>
								</select>
								<label>Chọn đội bóng tham gia</label>
							</div> 
							<div class="file-field input-field col s4"> 
								<div class="btn upload_avatar_player man-utd">
									<span>Upload Avatar</span>
									<input type="file" name="avatar_player">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text" style="height: 3rem">
								</div> 
							</div>  
						</div> 
						<div class="row name_team_row">
							<div class="input-field col s4"> 
								<input name="birthday" id="birthday" type="text" class="datepicker validate">
								<label for="birthday">Sinh Nhật</label>
							</div>  
							<div class="input-field col s4"> 
								<input name="goals" id="goals" type="text" class="validate">
								<label for="goals">Bàn thắng</label>
							</div> 
							<div class="input-field col s4"> 
								<input name="assists" id="assists" type="text" class="validate">
								<label for="assists">Kiến tạo</label>
							</div> 
						</div>
						<div class="row name_team_row">
							<div class="input-field col s3"> 
								<input name="position" id="position" type="text" class="validate">
								<label for="position">Vị Trí Thi Đấu</label>
							</div>
							<div class="input-field col s3"> 
								<input name="desc_position" id="desc_position" type="text" class="validate">
								<label for="desc_position">Vị Trí Viết Tắt</label>
							</div>  
							<div class="input-field col s3"> 
								<input name="squad_number" id="squad_number" type="text" class="validate">
								<label for="squad_number">Số Áo Thi Đấu</label>
							</div>
							<div class="input-field col s3">  
								<input name="girly" id="girly" type="text" class="validate">
								<label for="girly">Chỉ số nữ tính</label>
							</div> 
						</div>
						<div class="row">
							<div class="input-field col s12">
								<button class="waves-effect waves-light btn man-utd" name="ok">Submit</button>
							</div>
						</div> 
					</form>
				</div>
			</div>
		</div>  
	</div>
</main>