<main>
	<div class="row">
		<div class="col s12">
			<div style="padding: 35px;" align="center" class="card">
				<div class="row">
					<div class="left card-title">
						<b>Danh sách cầu thủ</b>
					</div> 
					<div class="alert">
						<span class="error"></span> 
						<span class="success"></span>
					</div> 
				</div>  
				<div class="row season_table"> 
					<div class="col s12 table_league_preview"> 
						<table class="striped custom-table">
							<thead>
								<tr> 
									<th></th>
									<th>Tên</th>
									<th>Ảnh</th>
									<th>Ngày sinh</th>
									<th>Số áo</th>
									<th>Bàn thắng</th>
									<th>Vị trí</th> 
									<th>Viết tắt</th>  
									<th style="text-align: center;">Chỉnh Sửa</th>
								</tr>
							</thead> 
							<tbody> 
								<?php foreach($list_player as $row) {?>
									<tr>  
										<td><?php echo $row->id?></td>
										<td><?php echo $row->name?></td>
										<td class="avatar_col">
											<?php if ($row->avatar_player != "") { ?> 
												<img src="<?php echo base_url()?>/uploads/avatar_player/<?php echo $row->avatar_player?>">
											<?php } else { ?> 
												<img src="<?php echo base_url()?>/uploads/avatar_player/avatar.png">
											<?php } ?>  
										</td> 
										<td><?php echo $row->birthday?></td>
										<td><?php echo $row->squad_number?></td>
										<td><?php echo $row->goals?></td>
										<td><?php echo $row->position?></td> 
										<td><?php echo $row->desc_position?></td> 
										<td style="text-align: center;"><a href="<?php echo admin_url('controller_player/edit_player/'.$row->id);?>"><i class="material-icons">edit</i></a></td> 
									</tr>  
								<?php }?>       
							</tbody>
						</table> 
						<div class="paging"><?php echo $paginator; ?></div> 
					</div>
				</div> 
			</div>
		</div>  
	</div>
</main>