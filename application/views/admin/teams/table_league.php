<main>
	<div class="row">
		<div class="col s12">
			<div style="padding: 35px;" align="center" class="card">
				<div class="row">
					<div class="left card-title">
						<b>Bảng xếp hạng</b>
					</div> 
					<div class="alert">
						<span class="error"></span> 
						<span class="success"></span>
					</div> 
				</div>  
				<div class="row season_table">
					<div class="col s12">
						<div class="left card-title">
							<p>Cati Premier League 2018-2019</p>
						</div> 
					</div>
					<div class="col s12 table_league_preview">
						<table class="striped custom-table">
							<thead>
								<tr>
									<th></th>
									<th>Tên Đội</th>
									<th>Thắng</th>
									<th>Hòa</th>
									<th>Thua</th>
									<th>Bàn Thắng</th>
									<th>Bàn Thua</th> 
									<th>Hiệu Số</th>
									<th>Điểm</th>
									<th style="text-align: center;">Chỉnh Sửa</th>
								</tr>
							</thead> 
							<tbody> 
								<tr> 
									<td>1</td>
									<td class="flex_flag">
										<span class="gdlr-team-flag"><img src="https://cdn.goodlayers.com/realsoccer/wp-content/uploads/2014/09/flag-12.png" alt=""></span> 
										<span>FC Trứng Gà</span>
									</td>
									<td>3</td>
									<td>0</td>
									<td>1</td>
									<td>15</td>
									<td>7</td>
									<td>8</td>
									<td>10</td>
									<td style="text-align: center;"><a href="#"><i class="material-icons">edit</i></a></td> 
								</tr>     
								<tr> 
									<td>2</td>
									<td class="flex_flag">
										<span class="gdlr-team-flag"><img src="https://cdn.goodlayers.com/realsoccer/wp-content/uploads/2014/09/flag-13.png" alt=""></span> 
										<span>FC Chiều Tà</span>
									</td>
									<td>3</td>
									<td>0</td>
									<td>1</td>
									<td>15</td>
									<td>7</td>
									<td>8</td>
									<td>10</td>
									<td style="text-align: center;"><a href="#"><i class="material-icons">edit</i></a></td> 
								</tr>    
								<tr> 
									<td>2</td>
									<td class="flex_flag">
										<span class="gdlr-team-flag"><img src="https://cdn.goodlayers.com/realsoccer/wp-content/uploads/2014/09/flag-14.png" alt=""></span> 
										<span>FC Hội Ngộ</span>
									</td>
									<td>3</td>
									<td>0</td>
									<td>1</td>
									<td>15</td>
									<td>7</td>
									<td>8</td>
									<td>10</td>
									<td style="text-align: center;"><a href="#"><i class="material-icons">edit</i></a></td> 
								</tr>     
							</tbody>
						</table> 
					</div>
				</div> 
			</div>
		</div>  
	</div>
</main>