<main>
	<div class="row">
		<div class="col s12">
			<div style="padding: 35px;" align="center" class="card">
				<div class="row">
					<div class="left card-title">
						<b>Sửa thông tin đội bóng </b>
					</div> 
					<div class="alert">
						<span class="error"></span> 
						<span class="success"></span>
					</div> 
				</div> 
				<div class="row add_league_title"> 
					<form class="col s12" method="post">
						<div class="row"> 
							<div class="input-field col s6"> 
								<input name="name_team" id="name_team" type="text" class="validate">
								<label for="name_team">Tên Đội Bóng</label>
							</div>
							<div class="input-field col s6"> 
								<select>
									<option value="" disabled >Chọn giải đấu tham gia</option>
									<option value="1" selected>Cati Premier League 2018 - 2019</option>
									<option value="2">Cati FA Cup</option>
									<option value="3">Cati Champion League</option>
								</select>
								<label>Chọn giải đấu tham gia</label>
							</div>
						</div>
						<div class="row upload-logo">
							<div class="file-field input-field col s6"> 
								<div class="btn upload_logo_home">
									<span>Upload logo đội bóng</span>
									<input type="file">
								</div>
								<div class="file-path-wrapper">
									<input class="file-path validate" type="text" style="height: 3rem">
								</div> 
							</div> 
							<div class="input-field col s6"> 
								<input name="played" id="played" type="text" class="validate">
								<label for="played">Số Trận Thi Đấu</label>
							</div>
						</div>
						<div class="row name_team_row">
							<div class="input-field col s4"> 
								<input name="team_win" id="team_win" type="text" class="validate">
								<label for="team_win">Số Trận Thắng</label>
							</div>
							<div class="input-field col s4"> 
								<input name="team_drawn" id="team_drawn" type="text" class="validate">
								<label for="team_drawn">Số Trận Hòa</label>
							</div>
							<div class="input-field col s4"> 
								<input name="team_lose" id="team_lose" type="text" class="validate">
								<label for="team_lose">Số Trận Thua</label>
							</div> 
						</div>
						<div class="row name_team_row">
							<div class="input-field col s4"> 
								<input type="text" id="goal_scored" name="goal_scored">
								<label for="goal_scored">Số Bàn Thắng</label>
							</div>
							<div class="input-field col s4"> 
								<input name="goal_conceded" id="goal_conceded" type="text" class="validate">
								<label for="goal_conceded">Số Bàn Thua</label>
							</div>
							<div class="input-field col s4"> 
								<input name="sum_goal" id="sum_goal" type="text" class="validate">
								<label for="sum_goal">Hiệu Số Bàn Thắng</label>
							</div> 
						</div>
						<div class="row">
							<div class="input-field col s12">
								<button class="waves-effect waves-light btn man-utd" name="ok">Submit</button>
							</div>
						</div> 
					</form>
				</div>
			</div>
		</div>  
	</div>
</main>